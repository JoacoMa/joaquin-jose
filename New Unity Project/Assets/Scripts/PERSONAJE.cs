﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*/*Comportamiento Pelea:
Deberá representar en forma abstracta un duelo entre dos personajes llamados Ken y Ryu.
Cada personaje deberá tener
como propiedades
1. Nombre
2. Vida
3. Defensa
4. Fuerza
5. Arma.*/

namespace Assets.Scripts
{
    public class Personaje
    {
        public string Nombre { get; set; }
        public int Vida {get; set;} 
 
       public Personaje (string nombre, int vida)

        {
            Nombre = nombre;
            Vida = vida;
        }
    }
}
