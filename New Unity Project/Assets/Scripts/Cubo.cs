﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine.Video;

/*Comportamiento Pelea:
Deberá representar en forma abstracta un duelo entre dos personajes llamados Ken y Ryu.
Cada personaje deberá tener
como propiedades
1. Nombre
2. Vida
3. Defensa
4. Fuerza
5. Arma

ARMA = Nombre y CapacidadDeAtaque.
Personajes tendrán armas distintes, atacar con pegar.
Daño inferido, proporcional a ladefensa y a la
capacidad de ataque del arma del atacante y fuerza. 
Ryu atacará cuando se presione la tecla W 
Ken cuando se presione la tecla P. 
Cuando uno de los personajes se quede sin Vida,
se imprimirá en la consola un mensaje de:

Game Over y el nombre del personaje que ganó la pelea.
 
 
 */

public class Cubo : MonoBehaviour
{
    Personaje God = new Personaje("Ken", 100);
    Personaje Evil = new Personaje("Ryu", 100);

    void Start()
    {

        God.Vida = 100;
        Evil.Vida = 100;
  
        Debug.Log(God + "=" + God.Vida);
    }
} 
public class Personaje
{
    public string Nombre { get; set; }
    public int Vida { get; set; }

    public Personaje(string nombre, int vida, int defensa, int fuerza, int arma)
    {

        Nombre = nombre;
        Vida = vida;

    }
    public override string ToString()
    {
        return "Mi nombre es: " + Nombre;
    }

}    
    /* Posibles cambios a implementar */

    // Parte en la que puedes ver un if de determinar el estado de los personajes con respecto a su vida
  
  /*  if (Vida > 0)
        { 
        Debug.Log ("Personaje puede atacar aun");
        }
        else
        {
        Debug.Log ( "Personaje ha sido derrotado");
    }

  */


    //

